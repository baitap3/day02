<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./a.css">
</head>


<body id="body">
    <form action="action_page.php" method="post">

        <div class="container">
            <?php
            $day_of_week = array("chủ nhật", "thứ hai", "thứ ba", "thứ tư", "thứ năm", "thứ sáu", "thứ bảy");
            function day_of_week(string $a, array $day_of_week)
            {
                $a = (int) $a;
                return $day_of_week[$a];
            }
            // setlocale(LC_TIME, 'fr_CA.UTF-8');
            // echo "Hello World!";
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            // $d = strtotime("today");
            $w = date('w');
            $h = date('H:i');
            $date = date('d/m/Y');

            $str = $h.", ".day_of_week($w, $day_of_week) . " ngày " . $date;
            echo "<div id='time'>Bây giờ là: {$str}</div>";
            // echo $date;
            //gettime()
            // echo "Thứ: " . $date['weekday'] . "<hr>";
            // echo "Ngày: " . $date['mday'] . "<hr>";
            // echo "Tháng: " . $date['mon'] . "<hr>";
            // echo "Năm: " . $date['year'] . "<hr>";
            // echo "Giờ: " . $date['hours'] . "<hr>";
            // echo "Phút: " . $date['minutes'] . "<hr>";
            // echo "Giây: " . $date['seconds'] . "<hr>";


            ?>
            <fieldset class="group_user">
                <label id="user" for="uname">Tên đăng nhập</label>
                <input id="user_input" type="text" name="uname" required>
            </fieldset>

            <fieldset class="group_pass">
                <label id="pass" for="psw">Mật khẩu</label>
                <input id="pass_input" type="password" name="psw" required>
            </fieldset>

            <fieldset class="login">
                <button id="my_button" type="submit">Đăng nhập</button>

                <!-- <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label> -->
            </fieldset>
        </div>

        <!-- <div class="container" style="background-color:#f1f1f1">
  <button type="button" class="cancelbtn">Cancel</button>
  <span class="psw">Forgot <a href="#">password?</a></span>
</div> -->
    </form>

</body>

</html>